License of media (textures)
--------------------------------------
Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)
http://creativecommons.org/licenses/by-sa/3.0/

Authors of media files
-----------------------
celeron55, Perttu Ahola <celeron55@gmail.com>:
  crack_anylength.png

BlockMen (CC BY-SA 3.0):
  bubble.png
  heart.png

Krock (CC BY-SA):
  player.png
  player_back.png

Everything else is under WTFPL.
